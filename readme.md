# 测试项目

- 安装命令
```
scrapy startproject joboble
```

- 创建一个新的spider
```
scrapy genspider jobbole blog.jobbole.com
```

- 图片下载需要的库
```
pip install pillow -i https://pypi.douban.com/simple
```

- 测试
```
scrapy shell https://www.baidu.com
```