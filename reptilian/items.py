# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
import datetime
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, TakeFirst, Identity, Join


class ReptilianItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


def date_convert(value):
    value = value.replace("·", "").strip()
    try:
        create_date = datetime.datetime.strptime(value, "%Y/%m/%d").date()
    except Exception as e:
        create_date = datetime.datetime.now().date()

    return create_date

def remove_comment_tag(value):
    if "评论" in value:
        return ""
    return value


class ArticleItemLoder(ItemLoader):
    default_output_processor = TakeFirst()


class JobBoleArticleItem(scrapy.Item):
    title = scrapy.Field()  # 标题
    create_date = scrapy.Field(
        input_processor=MapCompose(date_convert)
    )  # 创建的时间
    url = scrapy.Field()  # 来源连接
    img_url = scrapy.Field(
        output_processor=Identity()
    )  # 封面图
    front_image_url = scrapy.Field()
    tags = scrapy.Field(
        input_processor=MapCompose(remove_comment_tag),
        output_processor=Join(',')
    )  # 标签
    url_object_id = scrapy.Field()
    content = scrapy.Field()  # 内容


